package login;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;

import mvccontroll.LoginSite2Application;

import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = LoginSite2Application.class)
@WebAppConfiguration
public class LoginSite2ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
