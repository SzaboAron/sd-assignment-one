var ClientBox = React.createClass({
	getInitialState: function() {
	    return {transactor: '', reciever: '', ammount: ''};
	},
  	handleTransactorChange: function(e) {
		this.setState({transactor: e.target.value});
	},
	handleRecieverChange: function(e) {
		this.setState({reciever: e.target.value});
	},
	handleMoneyChange: function(e) {
		this.setState({ammount: e.target.value});
	},
	handleSubmit: function(e) {
		e.preventDefault();
	    var transactor = this.state.transactor.trim();
	    var reciever = this.state.reciever.trim();
	    var ammount = this.state.ammount.trim();
	   
	    var loginData = {transactor:transactor, reciever:reciever, ammount:ammount}
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		        this.setState({data: data});
		        if (data){
		        	window.location.href = "/viewclients";
		        }
		        else{
		        	alert("Some Data is Wrong!");
		        }
		        
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
	render: function() {
	    return (
	    		<form onSubmit={this.handleSubmit}>
			    <label htmlFor="fname">Transactor Personal Numerical Code</label>
			    <input 
			    type="text" 
			    id="fname" 
			    name="firstname"
			    value={this.state.transactor}
		        onChange={this.handleTransactorChange}	
				required/>
			
			    <label htmlFor="reciever">Reciever Personal Numerical Code</label>
			    <input 
			    type="text" 
			    id="reciever" 
			    name="reciever"
			    value={this.state.reciever}
		        onChange={this.handleRecieverChange}	
				required/>
			    	
		    	<label htmlFor="idcn">Ammount</label>
			    <input 
			    type="text" 
			    id="idcn"
			    name="money"
			    value={this.state.ammount}
		        onChange={this.handleMoneyChange}	
				required/>
			    	
			    <input type="submit" value="Start transaction"/>
			  </form>);
		}
	});

ReactDOM.render(
		 <ClientBox url="/trans"  />,
		  document.getElementById('transactionScript')
		);