var EmployeeForm = React.createClass({
	getInitialState: function() {
	    return {name: '', password: '', role:"ROLE_USER"};
	  },
	  handleUserNameChange: function(e) {
			this.setState({name: e.target.value});
		},
		handlePasswordChange: function(e) {
		    this.setState({password: e.target.value});
		},
		handleSubmit: function(e) {
			e.preventDefault();
		    var name = this.state.name.trim();
		    var password = this.state.password.trim();
		    var role = this.state.role.trim();
		    if (!name || !password || !role) {
		    	console.log(role);
		    	return false;
		    }
		    var loginData = {name: name, password: password, role: role}
		    console.log(loginData);
			  $.ajax({
				  headers: { 
			        'Accept': 'application/json',
			        'Content-Type': 'application/json' 
				  },
			      url: this.props.url,
			      dataType: 'json',
			      type: 'POST',
			      data: JSON.stringify(loginData),
			      success: function(data) {
			        this.setState({data: data});
			        if (data){
			        	window.location.href = "/";
			        }
			        else{
			        	alert("Username already taken!");
			        }
			        
			      }.bind(this),
			      error: function(xhr, status, err) {
			        console.error(this.props.url, status, err.toString());
			      }.bind(this)
			    });
		},
	 change: function(event){
         this.setState({role: event.target.value});
     },
	render: function() {
	    return (
	    		<form onSubmit={this.handleSubmit}>
			    <label>Username:</label>
			    <input 
				type="text" 
				placeholder="User Name" 
				value={this.state.userName}
		        onChange={this.handleUserNameChange}	
				required />
			
			    <label>Password:</label>
			    <input 
				type="password" 
				placeholder="Password" 
				value={this.state.password}
		        onChange={this.handlePasswordChange}
				required />
			    
			    <label htmlFor="role">Role:</label>
			    <select id="role" name="roleselect" onChange={this.change} value={this.state.role}>
			      <option value="ROLE_USER">USER</option>
			      <option value="ROLE_ADMIN">ADMINISTRATOR</option>
			    </select>
			
			    <input type="submit" value="Submit"/>
			  </form>);
		}
	});

ReactDOM.render(
		 <EmployeeForm url="/registeremployee"  />,
		  document.getElementById('employeeScript')
		);