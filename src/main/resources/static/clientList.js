var ClientList = React.createClass({
	getInitialState :function(){
		return {data:[],myClient:'',lastname: '', firstname: '', cardNumber: '',cnp: '', address: '', money: ''};
	},
	handleLastNameChange: function(e) {
		this.setState({lastname: e.target.value});
	},
	handleFirstNameChange: function(e) {
		this.setState({firstame: e.target.value});
	},
	handleCardNumberChange: function(e) {
		this.setState({cardNumber: e.target.value});
	},
	handleCnpChange: function(e) {
		this.setState({cnp: e.target.value});
	},
	handleAddressChange: function(e) {
		this.setState({address: e.target.value});
	},
	handleMoneyChange: function(e) {
		this.setState({money: e.target.value});
	},
	componentDidMount: function() {
	    $.ajax({
	      url: this.props.url1,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	alert("error");
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	    console.log(this.state.myClient);
	  },
	  handleSubmit: function(e,f) {
		  f.preventDefault();
		  var personalNumericalCode = e;
		  var name = {personalNumericalCode : personalNumericalCode};
		  console.log(name);
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(name),
		      success: function(data) {
		    	  this.setState({lastname: data.lastName});
		    	  this.setState({firstname: data.firstName});
		    	  this.setState({cardNumber: data.identityCardNumber});
		    	  this.setState({cnp: data.personalNumericalCode});
		    	  this.setState({address: data.address});
		    	  this.setState({money: data.money});
		    	  this.setState({myClient:data});
		    	  console.log(this.state.myClient)
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		
	 },
	 handleDelete: function(e,f) {
		  var personalNumericalCode = e;
		  var name = {personalNumericalCode : personalNumericalCode};
		  console.log(name);
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.deleteurl,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(name),
		      success: function(data) {
		    	  if (!data){
		    		  alert("Delete Unsuccesfull!")
		    	  }
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		
	 },
	 handleEdit: function(e){
		 //e.preventDefault();
		 var firstName = this.state.firstname.trim();
	    var lastName = this.state.lastname.trim();
	    var identityCardNumber = this.state.cardNumber.trim();
	    var personalNumericalCode = this.state.cnp.trim();
	    var address = this.state.address.trim();
	    var money = this.state.money.trim();
	    var loginData = {firstName: firstName, lastName: lastName, identityCardNumber: identityCardNumber, 
	    	    personalNumericalCode: personalNumericalCode, address: address, money: money }
		 $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url2,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		    	  this.setState({lastname: '', firstname: '', cardNumber: '',cnp: '', address: '', money: ''});
		    	  this.setState({myClient:data});
		    	  console.log(this.state.myClient)
		    	 
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		 
	 },
	  render: function() {
		  var that = this;	
		  var commentNodes = this.state.data.map(function(clients) {
		  		return(
		  		<tr>
		 	    <td>{clients.firstName}</td>
		  	    <td>{clients.lastName}</td> 
		  	    <td>{clients.personalNumericalCode}</td>
		  	    <td>{clients.money}</td>
		  	    <td>
		  	    	<form onSubmit={that.handleSubmit.bind(that,clients.personalNumericalCode)}>
		  	    		<input type="submit" value="Edit" /> 
		  	    	</form>
  	    		</td>
  	    		<td>
	  	    	<form onSubmit={that.handleDelete.bind(that,clients.personalNumericalCode)}>
	  	    		<input type="submit" value="Delete" /> 
	  	    	</form>
	    		</td>
		  	    </tr> );
		  	});
		  	if (this.state.myClient == ''){
			return (
					<table>
					<tbody>
					<tr>
					<th>First Name</th>
				    <th>Last Name</th> 
				    <th>CNP</th>
				    <th>Money</th>
				    <th>Edit</th>
				    <th>Delete</th>
				    </tr>
					{commentNodes}
					</tbody>
					</table>
		    );
		  	}else return(
		  			<form onSubmit={this.handleEdit}>
		  			 <label for="fname">First Name</label>
					    <input 
					    type="text" 
					    id="fname" 
					    name="firstname"
					    value={this.state.firstname}
				        onChange={this.handleFirstNameChange}	
						required/>
					
					    <label for="lname">Last Name</label>
					    <input 
					    type="text" 
					    id="lname" 
					    name="lastname"
					    value={this.state.lastname}
				        onChange={this.handleLastNameChange}	
						required/>
					    	
				    	<label for="idcn">Identity Card Number</label>
					    <input 
					    type="text" 
					    id="idcn"
					    name="Identity Card Number"
					    value={this.state.cardNumber}
				        onChange={this.handleCardNumberChange}	
						required/>
					    	
				    	<label for="cnp">Personal Numerical Code</label>
					    <input 
					    type="text" 
					    id="cnp" 
					    name="Personal Numerical Code"
					    value={this.state.cnp}
				        onChange={this.handleCnpChange}	
						required/>
					    	
					    <label for="addr">Address</label>
					    <input 
					    type="text" 
					    id="addr" 
					    name="addr"
					    value={this.state.address}
				        onChange={this.handleAddressChange}	
						required/>
					    
					    <label for="money">Deposited Money</label>
					    <input 
					    type="text" 
					    id="mon" 
					    name="money"
					    value={this.state.money}
				        onChange={this.handleMoneyChange}	
						required/>
					
					    <input type="submit" value="Submit"/>
				    </form>
		  	);
	
	  }
});

ReactDOM.render(
		  <ClientList url1="/viewclientsdata" url="/editclient" url2="/editclients" deleteurl="/deleteclients"  />,
		  document.getElementById('clientList')
		);