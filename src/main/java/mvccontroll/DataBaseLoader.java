package mvccontroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import data.MyUser;
import data.UserRepository;

@Component
public class DataBaseLoader implements CommandLineRunner {
	
	private final UserRepository repository;
	
	@Autowired
	public DataBaseLoader(UserRepository repository) {
		super();
		this.repository = repository;
	}
	
	@Override
	public void run(String... strings) throws Exception {

		//SecurityContextHolder.getContext().setAuthentication(
		//	new UsernamePasswordAuthenticationToken("Aron", "aron",
		//		AuthorityUtils.createAuthorityList("ROLE_ADMIN")));
		
		SecurityContextHolder.clearContext();
	}
}
