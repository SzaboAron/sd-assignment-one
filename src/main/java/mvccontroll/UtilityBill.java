package mvccontroll;

public class UtilityBill {

	private String personalNumericalCode;
	private String billId;
	private Long ammount;
	
	public UtilityBill(String personalNumericalCode, String billId, Long ammount) {
		super();
		this.personalNumericalCode = personalNumericalCode;
		this.billId = billId;
		this.ammount = ammount;
	}

	public UtilityBill() {
		super();
	}

	public String getPersonalNumericalCode() {
		return personalNumericalCode;
	}

	public void setPersonalNumericalCode(String personalNumericalCode) {
		this.personalNumericalCode = personalNumericalCode;
	}

	public String getBillId() {
		return billId;
	}

	public void setBillId(String billId) {
		this.billId = billId;
	}

	public Long getAmmount() {
		return ammount;
	}

	public void setAmmount(Long ammount) {
		this.ammount = ammount;
	}
	
	
}
