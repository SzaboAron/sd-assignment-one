package mvccontroll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import data.MyUser;
import data.UserRepository;

@Component
public class SpringDataJpaUserDetailsService implements UserDetailsService {
	
	private final UserRepository repository;

	@Autowired
	public SpringDataJpaUserDetailsService(UserRepository repository) {
		this.repository = repository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException,NullPointerException {
		MyUser myuser= this.repository.findByName(name);	
		return new User(myuser.getName(), myuser.getPassword(),
				AuthorityUtils.createAuthorityList(myuser.getRole()));
		
	}

}
