package mvccontroll;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan
@EnableJpaRepositories(basePackages = "data")
@EntityScan(basePackages = "data")
public class LoginSite2Application {

	public static void main(String[] args) {
		SpringApplication.run(LoginSite2Application.class, args);
	}
}
