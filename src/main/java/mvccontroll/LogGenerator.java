package mvccontroll;

public class LogGenerator {
	
	private String name;
	private String date;
	
	public LogGenerator() {
		super();
	}

	public LogGenerator(String name, String date) {
		super();
		this.name = name;
		this.date = date;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
}
