package mvccontroll;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class Transaction {
	
	private String transactor;
	private String reciever;
	private Long ammount;
	@Id
	@GeneratedValue
	private Long transactionId;
	
	public Transaction(String transactor, String reciever, Long ammount) {
		super();
		this.transactor = transactor;
		this.reciever = reciever;
		this.ammount = ammount;
	}

	public Transaction() {
		super();
	}

	public String getTransactor() {
		return transactor;
	}

	public void setTransactor(String transactor) {
		this.transactor = transactor;
	}

	public String getReciever() {
		return reciever;
	}

	public void setReciever(String reciever) {
		this.reciever = reciever;
	}

	public Long getAmmount() {
		return ammount;
	}

	public void setAmmount(Long ammount) {
		this.ammount = ammount;
	}

	public Long getTransactionId() {
		return transactionId;
	}
	
}
