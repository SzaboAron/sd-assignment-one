package mvccontroll;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {
    
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/home").setViewName("home");
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/newclient").setViewName("newclient");
        registry.addViewController("/viewclients").setViewName("viewclients");
        registry.addViewController("/newemployee").setViewName("newemployee");
        registry.addViewController("/viewemployee").setViewName("viewemployee");
        registry.addViewController("/transaction").setViewName("transaction");
        registry.addViewController("/bill").setViewName("bill");
        registry.addViewController("/generatelog").setViewName("generatelog");
    }
}