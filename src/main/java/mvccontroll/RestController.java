package mvccontroll;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import data.Client;
import data.ClientRepository;
import data.EmployeeLog;
import data.MyUser;
import data.UserRepository;


@Controller
public class RestController{

	@Autowired
	UserRepository userRepository;
	@Autowired
	ClientRepository clientRepository;
	
	
	@RequestMapping(value = "/registerClient", method = RequestMethod.POST)
	public @ResponseBody boolean regClient(@RequestBody Client client){
		if (client == null){
			System.out.println("Error");
			return false;
		}
		if (clientRepository.findByPersonalNumericalCode(client.getPersonalNumericalCode()) == null){
			clientRepository.save(client);
			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			MyUser logUser = userRepository.findByName(auth.getName());
			logUser.addToLog(new EmployeeLog(new Date(),
					"Registered a new Client: "+client.toString()));
			userRepository.save(logUser);
			
			return true;
		}
		return false;
	}
	
	@RequestMapping(value = "/viewclientsdata", method = RequestMethod.GET)
	public @ResponseBody List<Client> viewClients(@RequestBody(required = false) String name){
		if (name == null){
			return clientRepository.findAll();
		}
			else return clientRepository.findAll();
		
	}
	
	@RequestMapping(value = "/editclient", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody Client editingClient(@RequestBody Client personalNumericalCode){
		Client 	client = clientRepository.findByPersonalNumericalCode(personalNumericalCode.getPersonalNumericalCode());
		return client;
	}
	
	@RequestMapping(value = "/editclients", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody boolean editingClientfinal(@RequestBody Client newclient){
		Client 	client = clientRepository.findByPersonalNumericalCode(newclient.getPersonalNumericalCode());
		if (client == null) return false;
		client.setFirstName(newclient.getFirstName());
		client.setLastName(newclient.getLastName());
		client.setIdentityCardNumber(newclient.getIdentityCardNumber());
		client.setPersonalNumericalCode(newclient.getPersonalNumericalCode());
		client.setMoney(newclient.getMoney());
		clientRepository.save(client);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Edited Client: "+client.toString()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@Transactional
	@RequestMapping(value = "/deleteclients", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody boolean deleteClient(@RequestBody Client newclient){
		Client 	client = clientRepository.findByPersonalNumericalCode(newclient.getPersonalNumericalCode());
		if (client == null) return false;
		clientRepository.removeByPersonalNumericalCode(newclient.getPersonalNumericalCode());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Removed a client with Personal Numerical Code: "+ newclient.getPersonalNumericalCode()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@RequestMapping(value = "/registeremployee", method = RequestMethod.POST)
	public @ResponseBody boolean regEmployee(@RequestBody MyUser user){
		if (user == null){
			System.out.println("Error");
			return false;
		}
		if (userRepository.findByName(user.getName())== null){
			userRepository.save(user);
			
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			MyUser logUser = userRepository.findByName(auth.getName());
			logUser.addToLog(new EmployeeLog(new Date(),
					"Registered employee: "+user.toString()));
			userRepository.save(logUser);
			
			return true;
		}
		return false;
	}
	
	@RequestMapping(value = "/employeeData", method = RequestMethod.GET)
	public @ResponseBody List<MyUser> viewEmpl(){
		return userRepository.findAll();
		
	}
	
	@RequestMapping(value = "/editemployee", method = RequestMethod.POST)
	public @ResponseBody MyUser selectEmpl(@RequestBody MyUser user){
		return userRepository.findById(user.getId());
		
	}
	
	@Transactional
	@RequestMapping(value = "/deleteemployee", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody boolean deleteEmpl(@RequestBody MyUser user){
		MyUser toDelete = userRepository.findById(user.getId());
		if (toDelete == null) return false;
		
		userRepository.deleteById(toDelete.getId());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Deleted employee with id: "+toDelete.getId().toString()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@RequestMapping(value = "/editemp", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody boolean submitEditEmployee(@RequestBody MyUser user){
		MyUser toMod = userRepository.findById(user.getId());
		if (toMod == null) return false;
		toMod.setName(user.getName());
		toMod.setPassword(user.getPassword());
		toMod.setRole(user.getRole());
		userRepository.save(toMod);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Edited employee: "+toMod.toString()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@RequestMapping(value = "/trans", method = RequestMethod.POST)
	public @ResponseBody boolean transactionMethod(@RequestBody Transaction transaction){
		Client transactor = clientRepository.findByPersonalNumericalCode(transaction.getTransactor());
		Client reciever = clientRepository.findByPersonalNumericalCode(transaction.getReciever());
		if (transactor.getMoney() < transaction.getAmmount()) return false;
		reciever.setMoney(reciever.getMoney()+transaction.getAmmount());
		transactor.setMoney(transactor.getMoney()-transaction.getAmmount());
		clientRepository.save(transactor);
		clientRepository.save(reciever);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Made a transaction: from: "+transactor.getPersonalNumericalCode()+
				" to "+reciever.getPersonalNumericalCode()+"with an ammount of:"
						+transaction.getAmmount()+" with ID:"+transaction.getTransactionId()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@RequestMapping(value = "/utility", method = RequestMethod.POST)
	public @ResponseBody boolean utilityBillProcessing(@RequestBody UtilityBill bill){
		Client client = clientRepository.findByPersonalNumericalCode(bill.getPersonalNumericalCode());
		if (client == null) return false;
		if (client.getMoney() < bill.getAmmount()) return false;
		client.setMoney(client.getMoney() - bill.getAmmount());
		clientRepository.save(client);
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Processed Utility Bill for Client: "+client.toString()+
				", Bill ID:"+bill.getBillId()+", ammount: "+bill.getAmmount()));
		userRepository.save(logUser);
		
		return true;
	}
	
	@RequestMapping(value = "/genlog", method = RequestMethod.POST, produces="application/json")
	public @ResponseBody String ganerateEmployeeLog(@RequestBody LogGenerator lgen){
		
		SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
		Date d;
		try { 
	          d = ft.parse(lgen.getDate()); 
	      } catch (ParseException e) { 
	          System.out.println("Unparseable using " + ft);
	          return "";
	      }
		
		MyUser user = userRepository.findByName(lgen.getName());
		if (user == null) return "";
		List<EmployeeLog> log = user.getLog();
		
		String logstring="";
	
		for (EmployeeLog tmp: log){
			if (tmp.getDate().after(d)){
				logstring+=tmp.toString()+"\r\n,";
			}
		}
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		MyUser logUser = userRepository.findByName(auth.getName());
		logUser.addToLog(new EmployeeLog(new Date(),
				"Generated report for User: "+user.getName()));
		userRepository.save(logUser);
		
		System.out.println(logstring);
		return logstring;
	}

}
