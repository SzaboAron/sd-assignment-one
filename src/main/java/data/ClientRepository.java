package data;

import java.util.List;

import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface ClientRepository extends Repository<Client, Long> {
	
	Client save(Client client);
	Client findByLastName(String lastName);
	Client findByFirstName(String firstName);
	Client findByPersonalNumericalCode(String personalNumericalCode);
	Long removeByPersonalNumericalCode(String personalNumericalCode);
	List<Client> findAll();
}
