package data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Client {

	@Id
	@GeneratedValue
	private Long Id;
	private String lastName;
	private String firstName;
	private String identityCardNumber;
	private String personalNumericalCode;
	private String address;
	private Long money;
	
	public Client(String firstName,String lastName , String identityCardNumber, String personalNumericalCode, String address) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.identityCardNumber = identityCardNumber;
		this.personalNumericalCode = personalNumericalCode;
		this.address = address;
	}

	public Client() {
		super();
	}

	
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdentityCardNumber() {
		return identityCardNumber;
	}

	public void setIdentityCardNumber(String identityCardNumber) {
		this.identityCardNumber = identityCardNumber;
	}

	public String getPersonalNumericalCode() {
		return personalNumericalCode;
	}

	public void setPersonalNumericalCode(String personalNumericalCode) {
		this.personalNumericalCode = personalNumericalCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getMoney() {
		return money;
	}

	public void setMoney(Long money) {
		this.money = money;
	}

	@Override
	public String toString() {
		return "Client [lastName=" + lastName + ", firstName=" + firstName + ", identityCardNumber="
				+ identityCardNumber + ", personalNumericalCode=" + personalNumericalCode + ", address=" + address
				+ ", money=" + money + "]";
	}
		
}
