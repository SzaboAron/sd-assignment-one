package data;

import java.util.List;

import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface UserRepository extends Repository<MyUser, Long> {
	
	MyUser save(MyUser myuser);
	MyUser findByName(String Name);
	List<MyUser> findAll();
	MyUser findById(Long id);
	Long deleteById(Long id);

}
