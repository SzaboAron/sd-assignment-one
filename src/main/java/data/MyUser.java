package data;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class MyUser {
	
	private @Id @GeneratedValue Long id;
	private String name;
	private String password;
	private String role;
	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<EmployeeLog> log;
	
	public MyUser(String name, String password, String role) {

		this.name = name;
		this.password = password;
		this.role = role;
		
	}
	
	public MyUser() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public Long getId(){
		return id;
	}

	@Override
	public String toString() {
		return "MyUser [name=" + name + ", password=" + password + ", role=" + role + "]";
	}
	
	public EmployeeLog addToLog(EmployeeLog log){
		this.log.add(log);
		return log;
	}
	
	public List<EmployeeLog> getLog(){
		return log;
	}
}
