package data;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class EmployeeLog {
	
	private @Id @GeneratedValue Long id;
	private Date date;
	private String report;
	
	public EmployeeLog(Date date, String report) {
		super();	
		this.date = date;
		this.report = report;
	}

	public EmployeeLog() {
		super();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReport() {
		return report;
	}

	public void setReport(String report) {
		this.report = report;
	}

	@Override
	public String toString() {
		return date.toString() + ": " + report;
	}
	
	
	
}
