var ClientBox = React.createClass({
	getInitialState: function() {
	    return {lastname: '', firstname: '', cardNumber: '',cnp: '', address: '', money: ''};
	},
  	handleLastNameChange: function(e) {
		this.setState({lastname: e.target.value});
	},
	handleFirstNameChange: function(e) {
		this.setState({firstname: e.target.value});
	},
	handleCardNumberChange: function(e) {
		this.setState({cardNumber: e.target.value});
	},
	handleCnpChange: function(e) {
		this.setState({cnp: e.target.value});
	},
	handleAddressChange: function(e) {
		this.setState({address: e.target.value});
	},
	handleMoneyChange: function(e) {
		this.setState({money: e.target.value});
	},
	handleSubmit: function(e) {
		e.preventDefault();
	    var firstName = this.state.firstname.trim();
	    var lastName = this.state.lastname.trim();
	    var identityCardNumber = this.state.cardNumber.trim();
	    var personalNumericalCode = this.state.cnp.trim();
	    var address = this.state.address.trim();
	    var money = this.state.money.trim();
	   
	    var loginData = {firstName: firstName, lastName: lastName, identityCardNumber: identityCardNumber, 
	    personalNumericalCode: personalNumericalCode, address: address, money: money }
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		        this.setState({data: data});
		        if (data){
		        	//window.location.href = "/";
		        	this.setState({lastname: '', firstname: '', cardNumber: '',cnp: '', address: '', money: ''});
		        }
		        else{
		        	alert("Some Data is Wrong!");
		        }
		        
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
	render: function() {
	    return (
	    		<form onSubmit={this.handleSubmit}>
			    <label for="fname">First Name</label>
			    <input 
			    type="text" 
			    id="fname" 
			    name="firstname"
			    value={this.state.firstname}
		        onChange={this.handleFirstNameChange}	
				required/>
			
			    <label for="lname">Last Name</label>
			    <input 
			    type="text" 
			    id="lname" 
			    name="lastname"
			    value={this.state.lastname}
		        onChange={this.handleLastNameChange}	
				required/>
			    	
		    	<label for="idcn">Identity Card Number</label>
			    <input 
			    type="text" 
			    id="idcn"
			    name="Identity Card Number"
			    value={this.state.cardNumber}
		        onChange={this.handleCardNumberChange}	
				required/>
			    	
		    	<label for="cnp">Personal Numerical Code</label>
			    <input 
			    type="text" 
			    id="cnp" 
			    name="Personal Numerical Code"
			    value={this.state.cnp}
		        onChange={this.handleCnpChange}	
				required/>
			    	
			    <label for="addr">Address</label>
			    <input 
			    type="text" 
			    id="addr" 
			    name="addr"
			    value={this.state.address}
		        onChange={this.handleAddressChange}	
				required/>
			    
			    <label for="money">Deposited Money</label>
			    <input 
			    type="text" 
			    id="mon" 
			    name="money"
			    value={this.state.money}
		        onChange={this.handleMoneyChange}	
				required/>
			
			    <input type="submit" value="Submit"/>
			  </form>);
		}
	});

ReactDOM.render(
		 <ClientBox url="/registerClient"  />,
		  document.getElementById('clientScript')
		);