var ClientList = React.createClass({
	getInitialState :function(){
		return {data:[],myClient:'',name: '', password: '', role: '',id: ''};
	},
	handleNameChange: function(e) {
		this.setState({name: e.target.value});
	},
	handlePasswordChange: function(e) {
	    this.setState({password: e.target.value});
	},
	change: function(event){
        this.setState({role: event.target.value});
    },
	componentDidMount: function() {
	    $.ajax({
	      url: this.props.url1,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({data: data});
	      }.bind(this),
	      error: function(xhr, status, err) {
	    	alert("error");
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });
	    console.log(this.state.myClient);
	  },
	  handleSubmit: function(e,f) {
		  f.preventDefault();
		  var id = e;
		  var name = {id : id};
		  console.log(name);
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(name),
		      success: function(data) {
		    	  this.setState({name: data.name});
		    	  this.setState({password: data.password});
		    	  this.setState({role: data.role});
		    	  this.setState({id: data.id});
		    	  this.setState({myClient: data});
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		
	 },
	 handleDelete: function(e,f) {
		 //f.preventDefault(); 
		 var id = e;
		  var name = {id : id};
		  console.log(name);
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.deleteurl,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(name),
		      success: function(data) {
		    	  if (!data){
		    		  alert("Delete Unsuccesfull!")
		    	  }
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		
	 },
	 handleEdit: function(e){
		 //e.preventDefault();
		var name = this.state.name.trim();
	    var password = this.state.password.trim();
	    var role = this.state.role.trim();
	    var id = this.id.trim();
	    var loginData = {name:name, password:password, role:role, id:id}
		 $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url2,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		    	  this.setState({name: '', password: '', id: '',role: ''});
		    	  this.setState({myClient:''});
		    	  console.log(this.state.myClient)
		    	 
		      }.bind(this),
		      error: function(xhr, status, err) {
		    	  console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
		  return false;
		 
	 },
	  render: function() {
		  var that = this;	
		  var commentNodes = this.state.data.map(function(clients) {
		  		return(
		  		<tr>
		 	    <td>{clients.name}</td>
		  	    <td>{clients.role}</td>
		  	    <td>
		  	    	<form onSubmit={that.handleSubmit.bind(that,clients.id)}>
		  	    		<input type="submit" value="Edit" /> 
		  	    	</form>
  	    		</td>
  	    		<td>
	  	    	<form onSubmit={that.handleDelete.bind(that,clients.id)}>
	  	    		<input type="submit" value="Delete" /> 
	  	    	</form>
	    		</td>
		  	    </tr> );
		  	});
		  	if (this.state.myClient == ''){
			return (
					<table>
					<tbody>
					<tr>
					<th>Username</th>
				    <th>Role</th> 
				    <th>Edit</th>
				    <th>Delete</th>
				    </tr>
					{commentNodes}
					</tbody>
					</table>
		    );
		  	}else return(
		  			<form onSubmit={this.handleEdit}>
		  			 <label>Username:</label>
					    <input 
						type="text" 
						placeholder="User Name" 
						value={this.state.name}
				        onChange={this.handleNameChange}	
						required />
					
					    <label>Password:</label>
					    <input 
						type="password" 
						placeholder="Password" 
						value={this.state.password}
				        onChange={this.handlePasswordChange}
						required />
					    
					    <label htmlFor="role">Role:</label>
					    <select id="role" name="roleselect" onChange={this.change} value={this.state.role}>
					      <option value="ROLE_USER">USER</option>
					      <option value="ROLE_ADMIN">ADMINISTRATOR</option>
					    </select>
					    <input type="submit" value="Submit"/>
				    </form>
		  	);
	
	  }
});

ReactDOM.render(
		  <ClientList url1="/employeeData" url="/editemployee" url2="/editemp" deleteurl="/deleteemployee"  />,
		  document.getElementById('employeeList')
		);