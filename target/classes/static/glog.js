var LogBox = React.createClass({
	getInitialState: function() {
	    return {name: '', date: '', log: ''};
	},
  	handleNameChange: function(e) {
		this.setState({name: e.target.value});
	},
	handleDateChange: function(e) {
		this.setState({date: e.target.value});
	},
	handleSubmit: function(e) {
		e.preventDefault();
	    var name = this.state.name.trim();
	    var date = this.state.date.trim();
	    var loginData = {name:name, date:date}
	    
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'text',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		    	  this.setState({log:data});        
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
	render: function() {
	    if (this.state.log == ''){
		return (
	    		
	    		<form onSubmit={this.handleSubmit}>
			    <label htmlFor="fname">Employee Username</label>
			    <input 
			    type="text" 
			    id="fname" 
			    name="firstname"
			    value={this.state.name}
		        onChange={this.handleNameChange}	
				required/>
			    <label>Report after:</label>
			    <input 
			    type="date" 
			    name="logdate"
			    value = {this.state.date}
			    onChange={this.handleDateChange}
			    required/>
			    	
			    <input type="submit" value="Generate Log"/>
			  </form>);
	    }else return(
	    	<div>
	    	<h2>Report for {this.state.name}:</h2>
	    	<p>{this.state.log}</p>	
	    	<form>
	    	<input type="submit" value="Back"/>
	    	</form>
	    	</div>
	    );
		}
	});

ReactDOM.render(
		 <LogBox url="/genlog"  />,
		  document.getElementById('logScript')
		);