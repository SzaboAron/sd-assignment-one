var ClientBox = React.createClass({
	getInitialState: function() {
	    return {cnp: '', billid: '', ammount: ''};
	},
  	handleClientChange: function(e) {
		this.setState({cnp: e.target.value});
	},
	handleBillIdChange: function(e) {
		this.setState({billid: e.target.value});
	},
	handleMoneyChange: function(e) {
		this.setState({ammount: e.target.value});
	},
	handleSubmit: function(e) {
		e.preventDefault();
	    var personalNumericalCode = this.state.cnp.trim();
	    var billId = this.state.billid.trim();
	    var ammount = this.state.ammount.trim();
	   
	    var loginData = {personalNumericalCode:personalNumericalCode, billId:billId, ammount:ammount}
		  $.ajax({
			  headers: { 
		        'Accept': 'application/json',
		        'Content-Type': 'application/json' 
			  },
		      url: this.props.url,
		      dataType: 'json',
		      type: 'POST',
		      data: JSON.stringify(loginData),
		      success: function(data) {
		        if (data){
		        	window.location.href = "/viewclients";
		        }else alert("Transaction Unsuccessfull");		        
		      }.bind(this),
		      error: function(xhr, status, err) {
		        console.error(this.props.url, status, err.toString());
		      }.bind(this)
		    });
	},
	render: function() {
	    return (
	    		<form onSubmit={this.handleSubmit}>
			    <label htmlFor="fname">Personal Numerical Code</label>
			    <input 
			    type="text" 
			    id="fname" 
			    name="firstname"
			    value={this.state.cnp}
		        onChange={this.handleClientChange}	
				required/>
			
			    <label htmlFor="billid">Utility Bill Id</label>
			    <input 
			    type="text" 
			    id="billid" 
			    name="billid"
			    value={this.state.billid}
		        onChange={this.handleBillIdChange}	
				required/>
			    	
		    	<label htmlFor="idcn">Ammount</label>
			    <input 
			    type="text" 
			    id="idcn"
			    name="money"
			    value={this.state.ammount}
		        onChange={this.handleMoneyChange}	
				required/>
			    	
			    <input type="submit" value="Start transaction"/>
			  </form>);
		}
	});

ReactDOM.render(
		 <ClientBox url="/utility"  />,
		  document.getElementById('utilityScript')
		);